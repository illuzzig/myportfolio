const DigiLand = artifacts.require("DigiLand");
const truffleAssert = require("truffle-assertions");
let Web3 = require("Web3");
// https://blockheroes.dev/js-read-multiple-returned-values-solidity/

contract("Digiland", function (accounts) {
  let instance;

  beforeEach("should setup the contract instance", async () => {
    instance = await DigiLand.deployed();
  });

  it("addTokens", async function () {
    await instance.addToken(Web3.utils.utf8ToHex("Land"), {
      from: accounts[0],
    });
    await instance.addToken(Web3.utils.utf8ToHex("Home"), {
      from: accounts[0],
    });
    await instance.addToken(Web3.utils.utf8ToHex("Garden"), {
      from: accounts[0],
    });
    await instance.addToken(Web3.utils.utf8ToHex("Pool"), {
      from: accounts[0],
    });

    let result = await instance.getTokenIdByName(Web3.utils.utf8ToHex("Land"));
    let { 0: checkLand, 1: indexLand } = result;
    assert.equal(checkLand, true);
    assert.equal(indexLand, 0);

    result = await instance.getTokenIdByName(Web3.utils.utf8ToHex("Home"));
    let { 0: checkHome, 1: indexHome } = result;
    assert.equal(checkHome, true);
    assert.equal(indexHome, 1);
  });

  it("mint single token", async function () {
    await instance.mint(0, 1, Web3.utils.utf8ToHex("NULL"), {
      from: accounts[0],
    });

    await truffleAssert.reverts(
      instance.mint(0, 1, Web3.utils.utf8ToHex("NULL"), { from: accounts[0] })
    );
  });

  it("mint batch", async function () {
    await instance.mintBatch(
      [1, 2, 3],
      [1, 1, 1],
      Web3.utils.utf8ToHex("NULL"),
      {
        from: accounts[0],
      }
    );
  });

  it("setPrice and single buy", async function () {
    const price = 20;
    await instance.setPrice(3, price, {
      from: accounts[0],
    });

    await instance.setApprovalForAll(accounts[2], true, { from: accounts[0] });

    await instance.singleBuy(accounts[0], 3, Web3.utils.utf8ToHex("NULL"), {
      from: accounts[2],
      value: price,
    });

    assert.equal(
      await instance.ownerOf(accounts[2], 3, { from: accounts[2] }),
      true
    );
  });

  it("addItems", async function () {
    await instance.addLand(0, { from: accounts[0] });
    await instance.addItem(1, 0, { from: accounts[0] });
    await instance.addItem(2, 0, { from: accounts[0] });

    const landPrice = 15;
    const housePrice = 55;
    const gardenPrice = 25;

    await instance.setPrice(0, landPrice, {
      from: accounts[0],
    });

    await instance.setPrice(1, housePrice, {
      from: accounts[0],
    });

    await instance.setPrice(2, gardenPrice, {
      from: accounts[0],
    });
  });

  it("buyAll bulk", async function () {
    await instance.setApprovalForAll(accounts[1], true, { from: accounts[0] });
    await instance.bulkBuy(
      accounts[0],
      0,
      [1, 1, 1],
      Web3.utils.utf8ToHex("bulkBuy"),
      { from: accounts[1], value: 95 }
    );
    assert.equal(
      await instance.balanceOf(accounts[1], 0, { from: accounts[1] }),
      1
    );
    assert.equal(
      await instance.balanceOf(accounts[1], 1, { from: accounts[1] }),
      1
    );
    assert.equal(
      await instance.balanceOf(accounts[1], 2, { from: accounts[1] }),
      1
    );
  });
});
