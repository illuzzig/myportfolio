const ItemToken = artifacts.require("ItemToken");
const truffleAssert = require("truffle-assertions");
// web3.utils.asciiToHex(string)
contract("ItemToken", function (accounts) {
  let instance;
  let ipfsHash = "QmWWQSuPMS6aXCbZKpEjPHPUZN2NjB3YrhJTHsV4X3vb2t";
  beforeEach("should setup the contract instance", async () => {
    instance = await ItemToken.deployed();
  });

  it("should get main infos", async function () {
    assert.equal(await instance.name(), "HouseToken");
    assert.equal(await instance.symbol(), "HSE");
  });

  it("create", async function () {
    // success creation with unique ipfs hash
    await instance.create(ipfsHash, "", { from: accounts[0] });

    // it will fail - item exists with given ipfs hash
    await truffleAssert.reverts(
      instance.create(ipfsHash, "", {
        from: accounts[0],
      })
    );
  });

  it("allowBuy", async function () {
    // not the owner
    await truffleAssert.reverts(instance.allowBuy(1, 0, { from: accounts[2] }));
    // price zero
    await truffleAssert.reverts(instance.allowBuy(1, 0, { from: accounts[0] }));
    // pass
    await instance.allowBuy(1, 2, { from: accounts[0] });
    assert.equal(await instance.getPrice(1, { from: accounts[0] }), 2);
  });

  it("disallowBuy", async function () {
    // not the owner
    await truffleAssert.reverts(instance.disallowBuy(1, { from: accounts[2] }));
    // pass
    await instance.disallowBuy(1, { from: accounts[0] });
    assert.equal(await instance.getPrice(1, { from: accounts[0] }), 0);
  });

  it("buy", async function () {
    // This token is not for sale
    await truffleAssert.reverts(
      instance.buy(1, { from: accounts[2], value: 2 })
    );

    await instance.allowBuy(1, 2, { from: accounts[0] });
    // lower price
    await truffleAssert.reverts(
      instance.buy(1, { from: accounts[2], value: 1 })
    );

    await instance.approve(accounts[2], 1, { from: accounts[0] });

    const result = await instance.buy(1, { from: accounts[2], value: 2 });
    truffleAssert.eventEmitted(result, "ItemBought");
    // check transfer
    assert.equal(await instance.ownerOf(1), accounts[2]);
    assert.equal(await instance.getPrice(1, { from: accounts[2] }), 0);
  });
});
