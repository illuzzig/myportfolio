const LandToken = artifacts.require("LandToken");
const ItemToken = artifacts.require("ItemToken");
const truffleAssert = require("truffle-assertions");

contract("LandToken", function (accounts) {
  let landInstance;
  let itemInstance;

  beforeEach("should setup the contract instance", async () => {
    landInstance = await LandToken.deployed();
    itemInstance = await ItemToken.deployed();
  });

  it("should get main infos", async function () {
    assert.equal(await landInstance.name(), "LandToken");
    assert.equal(await landInstance.symbol(), "LND");
  });

  it("addItem", async function () {
    await landInstance.create(
      "QmWWQSuPMS6aXCbZKpEjPHPUZN2NjB3YrhJTHsV4X3vb2t",
      "",
      { from: accounts[2] }
    );

    await itemInstance.create(
      "QmWWQSuPMS6aXCbZKpEjPHPUZN2NjB3YrhJTHsV4X3vb3g",
      "",
      { from: accounts[0] }
    );

    await itemInstance.create(
      "QmWWQSuPMS6aXCbZKpEjPHPUZN2NjB3YrhJTHsV4X3vb3s",
      "",
      { from: accounts[2] }
    );

    await truffleAssert.reverts(
      landInstance.addItem(
        await landInstance.getNumberOfTokens({ from: accounts[0] }),
        await itemInstance.getNumberOfTokens({ from: accounts[0] }),
        await itemInstance.address,
        { from: accounts[0] }
      )
    );

    await truffleAssert.reverts(
      landInstance.addItem(
        await landInstance.getNumberOfTokens({ from: accounts[2] }),
        (await itemInstance.getNumberOfTokens({ from: accounts[2] })) - 1,
        await itemInstance.address,
        { from: accounts[2] }
      )
    );

    // pass
    await landInstance.addItem(
      await landInstance.getNumberOfTokens({ from: accounts[2] }),
      await itemInstance.getNumberOfTokens({ from: accounts[2] }),
      await itemInstance.address,
      { from: accounts[2] }
    );

    await truffleAssert.reverts(
      landInstance.addItem(
        await landInstance.getNumberOfTokens({ from: accounts[2] }),
        await itemInstance.getNumberOfTokens({ from: accounts[2] }),
        await itemInstance.address,
        { from: accounts[2] }
      )
    );
  });

  it("removeItem / buy", async function () {
    await truffleAssert.reverts(
      landInstance.removeItem(
        await landInstance.getNumberOfTokens({ from: accounts[2] }),
        (await itemInstance.getNumberOfTokens({ from: accounts[2] })) - 1,
        itemInstance.address,
        { from: accounts[2] }
      )
    );

    await itemInstance.create(
      "QmWWQSuPMS6aXCbZKpEjPHPUZN2NjB3YrhJTHsV4X3vbrr",
      "",
      { from: accounts[2] }
    );

    await truffleAssert.reverts(
      landInstance.removeItem(
        await landInstance.getNumberOfTokens({ from: accounts[2] }),
        await itemInstance.getNumberOfTokens({ from: accounts[2] }),
        itemInstance.address,
        { from: accounts[2] }
      )
    );

    await landInstance.removeItem(
      await landInstance.getNumberOfTokens({ from: accounts[2] }),
      (await itemInstance.getNumberOfTokens({ from: accounts[2] })) - 1,
      itemInstance.address,
      { from: accounts[2] }
    );

    await landInstance.allowBuy(
      await landInstance.getNumberOfTokens({ from: accounts[2] }),
      2,
      { from: accounts[2] }
    );

    await landInstance.approve(
      accounts[4],
      await landInstance.getNumberOfTokens({ from: accounts[2] }),
      { from: accounts[2] }
    );

    await landInstance.buy(
      await landInstance.getNumberOfTokens({ from: accounts[4] }),
      { from: accounts[4], value: 2 }
    );

    assert.equal(
      await landInstance.ownerOf(
        await landInstance.getNumberOfTokens({ from: accounts[4] }),
        { from: accounts[4] }
      ),
      accounts[4]
    );
  });
  it("buyAll", async function () {
    await landInstance.create(
      "QmWWQSuPMS6aXCbZKpEjPHPUZN2NjB3YrhJTHsV4X3vbrf",
      "",
      { from: accounts[7] }
    );

    await itemInstance.create(
      "QmWWQSuPMS6aXCbZKpEjPHPUZN2NjB3YrhJTHsV4X3vb344",
      "",
      { from: accounts[7] }
    );

    await itemInstance.create(
      "QmWWQSuPMS6aXCbZKpEjPHPUZN2NjB3YrhJTHsV4X3vb345",
      "",
      { from: accounts[7] }
    );

    await landInstance.addItem(
      await landInstance.getNumberOfTokens({ from: accounts[7] }),
      await itemInstance.getNumberOfTokens({ from: accounts[7] }),
      await itemInstance.address,
      { from: accounts[7] }
    );

    await landInstance.addItem(
      await landInstance.getNumberOfTokens({ from: accounts[7] }),
      (await itemInstance.getNumberOfTokens({ from: accounts[7] })) - 1,
      await itemInstance.address,
      { from: accounts[7] }
    );

    const num_items = await itemInstance.getNumberOfTokens({
      from: accounts[7],
    });
    const landId = await landInstance.getNumberOfTokens({ from: accounts[7] });

    await itemInstance.allowBuy(num_items, 2, { from: accounts[7] });
    await itemInstance.allowBuy(num_items - 1, 4, { from: accounts[7] });
    await landInstance.allowBuy(landId, 5, { from: accounts[7] });

    assert(itemInstance.getPrice(num_items), 2);
    assert(itemInstance.getPrice(num_items - 1), 4);
    assert(landInstance.getPrice(landId), 5);


    await itemInstance.setApprovalForAll(landInstance.address, true, { from: accounts[7] });
    await landInstance.buyAll(landId, { from: accounts[8], value: 11 });

    assert.equal(await landInstance.ownerOf(landId), accounts[8]);
    assert.equal(await itemInstance.ownerOf(num_items), accounts[8]);
    assert.equal(await itemInstance.ownerOf(num_items - 1), accounts[8]);
  });
});
