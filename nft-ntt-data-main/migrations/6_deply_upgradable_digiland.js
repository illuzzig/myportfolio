const { deployProxy } = require('@openzeppelin/truffle-upgrades');

const DigiLand = artifacts.require('DigiLand');

module.exports = async function (deployer) {
  await deployProxy(DigiLand, { deployer, initializer: 'init' });
};