const { deployProxy } = require('@openzeppelin/truffle-upgrades');

const LandToken = artifacts.require('LandToken');

module.exports = async function (deployer) {
  await deployProxy(LandToken,["LandToken", "LND"], { deployer, initializer: 'init' });
};