const { deployProxy } = require('@openzeppelin/truffle-upgrades');

const ItemToken = artifacts.require('ItemToken');

module.exports = async function (deployer) {
  await deployProxy(ItemToken,["HouseToken", "HSE"], { deployer, initializer: 'init' });
};