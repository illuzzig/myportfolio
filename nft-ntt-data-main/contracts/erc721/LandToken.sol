// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;
import "./ItemToken.sol";

contract LandToken is ItemToken {

    // landId -> sc address -> tokeId
    mapping(uint256 => mapping(address => uint256[])) internal itemsMap;
    // landId -> list of sc address
    mapping(uint256 => address[]) internal itemAddress;

    function _isAssociatedWithItems(uint256 _landId) isNotPaused view internal returns (bool)
    {
        for (uint256 i = 0; i < itemAddress[_landId].length; i++) {
            if (itemsMap[_landId][itemAddress[_landId][i]].length > 0) {
                return true;
            }
        }
        return false;
    }

    function buy(uint256 _landId) isNotPaused public payable override  {
        require(!_isAssociatedWithItems(_landId),"Land associated with items");
        super.buy(_landId);
    }

    function _hasItem(
        uint256 landId,
        uint256 tokenId,
        address tokenAddress
    ) isNotPaused view internal returns (bool, uint256) {
        for (uint256 i = 0; i < itemsMap[landId][tokenAddress].length; i++) {
            if (itemsMap[landId][tokenAddress][i] == tokenId) {
                return (true, i);
            }
        }
        return (false, 0);
    }

    function _hasAddress(uint256 _landId, address _tokenAddress)
        isNotPaused
        view
        internal
        returns (bool, uint256)
    {
        for (uint256 i = 0; i < itemAddress[_landId].length; i++) {
            if (itemAddress[_landId][i] == _tokenAddress) {
                return (true, i);
            }
        }
        return (false, 0);
    }

    function addItem(
        uint256 _landId,
        uint256 _tokenId,
        address _tokenAddress
    ) isNotPaused public {
        ItemToken item = ItemToken(_tokenAddress);
        require(_msgSender() == item.ownerOf(_tokenId) && _msgSender() == this.ownerOf(_landId), "Not the land owner or item owner");
        (bool checkItem, ) = _hasItem(_landId, _tokenId, _tokenAddress);
        require(!checkItem, "Land associated with the item");
        (bool checkAddress, ) = _hasAddress(_landId, _tokenAddress);
        if (!checkAddress) {
            itemAddress[_landId].push(_tokenAddress);
        }
        itemsMap[_landId][_tokenAddress].push(_tokenId);
    }

    function _remove(
        uint256 _landId,
        address _tokenAddress,
        uint256 _index
    ) isNotPaused internal {
        itemsMap[_landId][_tokenAddress][_index] = itemsMap[_landId][_tokenAddress][
            itemsMap[_landId][_tokenAddress].length - 1
        ];
        itemsMap[_landId][_tokenAddress].pop();

        (bool checkAddress, uint256 idx) = _hasAddress(_landId, _tokenAddress);
        if (itemsMap[_landId][_tokenAddress].length == 0 && checkAddress) {
            itemAddress[_landId][idx] = itemAddress[_landId][
                itemAddress[_landId].length - 1
            ];
            itemAddress[_landId].pop();
        }
    }

    function removeItem(
        uint256 _landId,
        uint256 _tokenId,
        address _tokenAddress
    ) isNotPaused public {
        ItemToken item = ItemToken(_tokenAddress);
        require(
            _msgSender() == item.ownerOf(_tokenId) &&
                _msgSender() == ownerOf(_landId),
            "Not the land or item owner"
        );
        (bool res, uint256 index) = _hasItem(_landId, _tokenId, _tokenAddress);
        require(res, "Land not associated with the item");
        _remove(_landId, _tokenAddress, index);
    }

    function buyAll(uint256 _landId) isNotPaused public payable {
        require(getPrice(_landId) > 0, "This Land is not for sales");
        address seller = this.ownerOf(_landId);
        uint256 tot = 0;
        for (uint256 i = 0; i < itemAddress[_landId].length; i++) {
            address tokenAddress = itemAddress[_landId][i];
            uint256[] memory items = itemsMap[_landId][tokenAddress];
            for (uint256 j = 0; j < items.length; j++) {
                ItemToken item = ItemToken(tokenAddress);
                uint256 amount = item.getPrice(items[j]);
                require(amount > 0, "zero price item");
                tot += amount;
                item.transferFrom(seller, _msgSender(), items[j]);
            }
        }
        tot += getPrice(_landId);
        require(tot == msg.value, "Incorrect price value");
        payable(seller).transfer(msg.value);
        _transfer(seller, _msgSender(), _landId);
    }
}
