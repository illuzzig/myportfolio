// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721URIStorageUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract ItemToken is ERC721URIStorageUpgradeable, OwnableUpgradeable {
    event ItemBought(address seller, address buyer, uint256 price);

    using Counters for Counters.Counter;
    Counters.Counter internal _tokenIds;
    
    bool internal paused;
    string internal uri;
    string internal version;
    mapping(string => bool) internal ipfsHashes;
    mapping(uint256 => uint256) internal tokenIdToPrice;

    // @constructor
    function init(string memory _name, string memory _symbol) initializer public {
        uri = "https://ipfs.io/ipfs/";
        paused = false;
        __ERC721_init(_name, _symbol);
        __Ownable_init();
    }
    function setURI(string memory _newuri) public onlyOwner {
        uri = _newuri;
    }

    function getUri() view public returns(string memory) {
        return uri;
    }

    function setVersion(string memory _version) isNotPaused onlyOwner public {
        version = _version;
    }

    function getVersion() view public returns(string memory){
        return version;
    }

    function setPaused(bool _paused) public onlyOwner {
        require(_msgSender() == owner(), "You are not the owner");
        paused = _paused;
    }

    modifier isNotPaused() {
        require(paused == false, "Contract Paused");
        _;
    }

    function create(string memory _hash, string memory _metadata) isNotPaused public returns (uint256) {
        require(!ipfsHashes[_hash], "Item exists with given ipfs hash");
        ipfsHashes[_hash] = true;
        _tokenIds.increment();
        uint256 newItemId = _tokenIds.current();
        _safeMint(_msgSender(), newItemId);
        _setTokenURI(newItemId, _metadata);
        return newItemId;
    }

    function allowBuy(uint256 _tokenId, uint256 _price) isNotPaused public {
        require(_msgSender() == ownerOf(_tokenId), "Not owner of this token");
        require(_price > 0, "Price zero");
        tokenIdToPrice[_tokenId] = _price;
    }

    function getPrice(uint256 _tokenId) isNotPaused view public returns(uint256) {
        return tokenIdToPrice[_tokenId];
    }

    function disallowBuy(uint256 _tokenId) isNotPaused public {
        require(_msgSender() == ownerOf(_tokenId), "Not owner of this token");
        tokenIdToPrice[_tokenId] = 0;
    }
    
    function buy(uint256 _tokenId) isNotPaused public virtual payable {
        require(msg.value == tokenIdToPrice[_tokenId], "Incorrect price value");
        address seller = ownerOf(_tokenId);
        transferFrom(seller, _msgSender(), _tokenId);
        tokenIdToPrice[_tokenId] = 0;
        payable(seller).transfer(msg.value);
        emit ItemBought(seller, _msgSender(), msg.value);
    }

    function getNumberOfTokens() isNotPaused view public returns(uint256) {
        return _tokenIds.current();
    }
}