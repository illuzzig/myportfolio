// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts-upgradeable/token/ERC1155/ERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

contract DigiLand is Initializable, ERC1155Upgradeable, OwnableUpgradeable {
    event ItemBought(address seller, address buyer, uint256 price);
    bool internal paused;

    bytes32[] internal tokenList;
    // owner -> tokenId -> price
    mapping(address => mapping(uint256 => uint256)) internal itemPrice;
    // owner -> landId -> list of items
    mapping(address => mapping(uint256 => uint256[])) internal itemsList;
    // owner -> landIds
    mapping(address => uint256[]) internal landList;

    function init() public initializer {
        __ERC1155_init("https://ipfs.io/ipfs/");
        __Ownable_init();
    }

    function getTokenCount() view public returns (uint) {
        return tokenList.length;
    }

    function ownerOf(address owner, uint256 tokenId) public view returns (bool) {
        return balanceOf(owner, tokenId) != 0;
    }

    function getTokenName(uint index) view public returns (bytes32) {
        require(index < tokenList.length, "Index out of bound");
        return tokenList[index];
    }

    function addToken(bytes32 _name) isNotPaused public {
        (bool exists, ) = getTokenIdByName(_name);
        if(!exists) {
            tokenList.push(_name);
        }
    }

    function getTokenIdByName(bytes32 _name) view public returns(bool, uint256){
        for(uint256 i = 0; i < tokenList.length; i++) {
            if(keccak256(abi.encodePacked(tokenList[i])) == keccak256(abi.encodePacked(_name))) {
                return (true, i);
            }
        }
        return (false, 0);
    }

    function setPaused(bool _paused) onlyOwner public {
        require(_msgSender() == owner(), "You are not the owner");
        paused = _paused;
    }

    modifier isNotPaused() {
        require(paused == false, "Contract Paused");
        _;
    }

    function setURI(string memory _newuri) onlyOwner public {
        _setURI(_newuri);
    }

    function mint(uint256 _id, uint256 _amount, bytes memory _data) isNotPaused public {
        require(_id < getTokenCount(), "Token does not exist");
        require(_amount == 1, "Amount must be equal to one");
        require(balanceOf(msg.sender, _id) < 1, "balanceOf greater than one");
        _mint(msg.sender, _id, _amount, _data);
    }

    function checkAmount(uint256[] memory _amounts) pure private returns(bool) {
        for(uint256 i = 0; i < _amounts.length; i++) {
            if(_amounts[i] != 1) {
                return false;
            }
        }
        return true;
    }

    function checkId(uint256[] memory _ids) view private returns(bool) {
            for(uint256 i = 0; i < _ids.length; i++) {
            if(_ids[i] >= getTokenCount()) {
                return false;
            }
        }
        return true;
    }

    function checkBalance(address owner, uint256[] memory _ids) view private returns(bool) {
        for(uint256 i = 0; i< _ids.length; i++){
            if(balanceOf(owner, _ids[i]) > 1) {
                return false;
            }
        }
        return true;
    }

    function mintBatch(uint256[] memory _ids, uint256[] memory _amounts, bytes memory _data) isNotPaused public {
        require(checkId(_amounts), "Token id does not exist");
        require(checkAmount(_amounts), "Each amount must be equal to one");
        require(checkBalance(msg.sender, _ids), "balanceOf greater than one");
        _mintBatch(msg.sender, _ids, _amounts, _data);
    }

    function landExists(address owner, uint256 tokenId) view public returns(bool, uint256) {
        for(uint256 i = 0; i < landList[owner].length; i++) {
            if(tokenId == landList[owner][i]){
                return (true, i);
            }
        }
        return (false, 0);
    }

    function addLand(uint256 tokenId) isNotPaused public {
        require(ownerOf(msg.sender, tokenId), "Not the owner of this token");
        (bool res, ) = landExists(msg.sender, tokenId);
        require(!res, "Land exist");
        landList[msg.sender].push(tokenId);
    }

    function setPrice(uint256 tokenId, uint256 price) isNotPaused public {
        require(ownerOf(msg.sender, tokenId), "Not the owner of this token");
        itemPrice[msg.sender][tokenId] = price;
    }

    function _hasItem(address owner, uint256 landId, uint256 tokenId) view internal returns(bool, uint256){
        for(uint256 i = 0; i< itemsList[owner][landId].length; i++){
            if(itemsList[owner][landId][i] == tokenId){
                return (true, i);
            }
        }
        return (false, 0);

    }

    function addItem(uint256 tokenId, uint256 landId) isNotPaused public {
        require(ownerOf(msg.sender, tokenId) && ownerOf(msg.sender, landId), "Not the land owner or item owner");
        (bool checkLand,) = landExists(msg.sender, landId);
        require(checkLand, "Land not added");
        (bool ceckItem,) = landExists(msg.sender, tokenId);
        require(!ceckItem, "Can't add this item. It's a land");
        for(uint256 i = 0; i< landList[msg.sender].length; i++){
            (bool resp, ) = _hasItem(msg.sender, landList[msg.sender][i], tokenId);
            require(!resp, "Land associated with the item");
        }
        itemsList[msg.sender][landId].push(tokenId);
    }

    function removeItem(uint256 tokenId, uint256 landId) isNotPaused public {
         require(ownerOf(msg.sender, tokenId) && ownerOf(msg.sender, landId), "Not the land owner or item owner");
         (bool checkLand,) = landExists(msg.sender, landId);
         (bool resp, uint256 index) = _hasItem(msg.sender, landId, tokenId);
         if(checkLand && resp) {
            itemsList[msg.sender][landId][index] = itemsList[msg.sender][landId][itemsList[msg.sender][landId].length -1];
            itemsList[msg.sender][landId].pop();
         }
    }

    function singleBuy(address seller, uint256 tokenId, bytes memory data) isNotPaused public payable {
          require(ownerOf(seller, tokenId), "Not the owner of this token");
          uint256 price = itemPrice[seller][tokenId];
          require(price > 0, "zero price item");
          require(msg.value == price, "Incorrect price value");
          (bool checkLand, uint256 landIndex) = landExists(seller, tokenId);
          if(checkLand) {
              require(itemsList[seller][tokenId].length == 0, "land with items");
          }
          safeTransferFrom(seller, msg.sender, tokenId, 1, data);
          itemPrice[msg.sender][tokenId] = 0;
          payable(seller).transfer(msg.value);
          if(checkLand) {
              landList[seller][landIndex] = landList[seller][landList[seller].length -1];
              landList[seller].pop();
          }
          emit ItemBought(seller, msg.sender, price);
    }

    function _changeItemOwnerShip(uint256 landId, address seller, address newOwner) public{
        for(uint256 i = itemsList[seller][landId].length -1; i>=0; i--) {
            itemsList[newOwner][landId].push(itemsList[seller][landId][i]);
            itemsList[seller][landId].pop();
        }
    }

    function bulkBuy(address seller, uint256 landId, uint256[] memory _amounts, bytes memory _data) isNotPaused public payable {
        uint256 totalPrice = 0;
        (bool checkLand, uint256 landIndex) = landExists(seller, landId);
        require(checkLand, "land doesn't exist");
        itemsList[seller][landId].push(landId);
        for(uint256 i = 0; i< itemsList[seller][landId].length; i++){
            uint256 amount = itemPrice[seller][itemsList[seller][landId][i]];
            require(amount > 0, "zero price item");
            totalPrice+= amount;
        }
        require(totalPrice == msg.value, "Incorrect price value");
        payable(seller).transfer(msg.value);
        safeBatchTransferFrom(seller, msg.sender, itemsList[seller][landId], _amounts, _data);
        // cleaning
        itemsList[seller][landId].pop();
        if(checkLand) {
            landList[seller][landIndex] = landList[seller][landList[seller].length -1];
            landList[seller].pop();
        }
        landList[msg.sender].push(landId);
        _changeItemOwnerShip(landId, seller, msg.sender);
        emit ItemBought(seller, msg.sender, totalPrice);
    }
}