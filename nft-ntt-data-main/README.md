# ERC-721 Vs ERC-1155
The latest booming area of the  crypto industry is Non-fungible tokens(NFT). NFTs are famously gaining popularity in the gaming and collectible  space. When games on the Ethereum blockchain started emerging, companies looked for ways to progress and improve the standard of blockchain gaming. As a result, non-fungible tokens using the ERC721 standard were formed and Cryptokitties was one of the pioneer games which showed exactly how amazing this development was. However, NFTs can be used in a wide variety of sectors, from the art industry to within music, fashion, and much more.

Since then, the blockchain gaming space is moving forward and getting immense and magnificent development thus far. Enjin has created a new and approved Ethereum token standard, ERC1155.

The ERC-1155 token standard was created to function like a vending machine, where developers could deploy a single smart contract that can be used to mint unlimited fungible (identical) tokens and non-fungible (unique) tokens.

So, If you’re thirsty for more, let me take you further down the digital collectible rabbit hole with a look at the ERC-721 & ERC-1155 standards. 

**Fungibles & Non-Fungibles**
- ERC-721: Limited to only Non-Fungibles
- ERC-1155: Allows both Fungibles and Non-Fungibles
- ERC-1155: Allows new concepts like semi-fungible tokens. For example, fungible tokens could be “transmuted” into non-fungibles or vice-versa.

**Batch transfers**
- ERC-721: Supports transferring one token at a time
- ERC-1155: Supports batch transfers of many token IDs in a single transaction.
- Since each Ethereum transaction takes around 15-30 seconds, ERC-721 takes a long time to transfer many tokens, while ERC-1155 can send hundreds of different tokens in a single block.
- Certain optimized implementations of ERC-1155 have been tested for transfers of up to 150-200 tokens per second.
 
**Single smart contract, no data waste**
- ERC-721: Requires a new smart contract deployed for each new type of token (for example, a contract for CryptoKitties, another contract for CryptoCuties, etc)
- ERC-1155: Can be deployed in a single smart contract for infinite token types.
- ERC-1155 tokens cannot be accidentally locked in a contract that doesn’t support ERC-1155. You can only send tokens to an ERC-1155 capable contract! This prevents accidental token loss – your tokens are reverted immediately if the recipient doesn’t support ERC-1155. ERC-721 tried achieving this but it ended up being useless, because they included legacy “non-safe” functions in the standard. If a wallet uses the non-safe transfer method, your tokens could be permanently lost.

**Trigger smart contracts by sending a token**
- Sending an ERC-1155 token to a smart contract can trigger a chain of functions and events on the blockchain. For example, an exchange (DEX) could allow users to directly send tokens and the exchange to another token would be done atomically, instantly, on-chain.
- This is 100% reliable with ERC-1155 because of the strict “onERC1155Received” rules that these tokens follow.
- You cannot rely on ERC-721 tokens being sent using this method, because it supports non-safe transfers. If the wallet sends the token to that exchange using a non-safe transfer, it will be lost to the user.
- This means that dapps cannot rely on the ERC-721 receiver functions behaving as intended. These apps won’t be created, since they’re unreliable with ERC-721.

**Localization**
- ERC-721: Support only 1 language
- ERC-1155: Supports localization of languages for all metadata, such as the token name, description, or even the token image. This makes all tokens universal.

**ID Substitution**
- ERC-721: Supports only static metadata, so each token ID must have its metadata URI stored or managed by the smart contract.
- ERC-1155: Contracts can point to an infinite number of token URIs without storing any additional data on-chain. This can even be used to point to a web service that hosts dynamically generated token JSON for each token in a database.

**Rich Event Logs**
- ERC-721: Emits transfers and approvals.
- ERC-1155: Includes standardized events for mints, burns, transfers, approvals, and metadata changes. This permits cool ecosystem benefits like deep data analytics on tokens and rich token explorers.