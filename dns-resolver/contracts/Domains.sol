// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import {StringUtils} from "./utils/StringUtils.sol";

contract Domains is ERC721URIStorage {
    error Unauthorized();
    error AlreadyRegistered();
    error InvalidName(string name);

    string internal tld;
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;

    address payable public owner;

    mapping(string => address) private domains;
    mapping(string => string) private records;
    mapping(uint256 => string) private names;

    function getAllNames() public view returns (string[] memory) {
        string[] memory allNames = new string[](_tokenIds.current());
        for (uint256 i = 0; i < _tokenIds.current(); i++) {
            allNames[i] = names[i];
        }

        return allNames;
    }

    constructor(string memory _tld) payable ERC721("LLZ Name Service", "LNS") {
        owner = payable(_msgSender());
        tld = _tld;
    }

    function isOwner() public view returns (bool) {
        return _msgSender() == owner;
    }

    modifier onlyOwner() {
        require(isOwner());
        _;
    }

    function withdraw() public onlyOwner {
        uint256 amount = address(this).balance;

        (bool success, ) = _msgSender().call{value: amount}("");
        require(success, "Failed to withdraw Matic");
    }

    // This function will give us the price of a domain based on length
    function price(string memory name) public pure returns (uint256) {
        uint256 len = StringUtils.strlen(name);
        require(len > 0);
        if (len == 3) {
            return 5 * 10**17; // 0.5 MATIC
        } else if (len == 4) {
            return 3 * 10**17;
        } else {
            return 1 * 10**17;
        }
    }

    function isValid(string memory name) public pure returns (bool) {
        return StringUtils.strlen(name) >= 3 && StringUtils.strlen(name) <= 10;
    }

    function register(string memory name, string memory metadata)
        public
        payable
    {
        if (domains[name] != address(0)) revert AlreadyRegistered();
         if (!isValid(name)) revert InvalidName(name);
        require(msg.value >= price(name), "Domains: Not enough Matic paid");

        _tokenIds.increment();
        uint256 newItemId = _tokenIds.current();
        _safeMint(_msgSender(), newItemId);
        _setTokenURI(newItemId, metadata);
        domains[name] = _msgSender();
        names[newItemId] = name;
    }

    function getAddress(string memory name) public view returns (address) {
        require(
            domains[name] == _msgSender(),
            "Domains: not the owner of this domain"
        );
        return domains[name];
    }

    function setRecord(string memory name, string calldata record) public {
        if (_msgSender() != domains[name]) revert Unauthorized();
        records[name] = record;
    }

    function getRecord(string memory name) public view returns (string memory) {
        return records[name];
    }
}
