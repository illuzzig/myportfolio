const main = async () => {
  const domainContractFactory = await hre.ethers.getContractFactory("Domains");
  // We pass in "ninja" to the constructor when deploying
  const domainContract = await domainContractFactory.deploy("llz");
  await domainContract.deployed();

  console.log("Contract deployed to:", domainContract.address);

  let txn = await domainContract.register(
    "devopstech",
    "https://gateway.pinata.cloud/ipfs/Qmc2sMG9XfURa7XSMD4VfedCfRthFC4JtMSojSM1tnhWzv",
    { value: hre.ethers.utils.parseEther("0.1") }
  );
  await txn.wait();

  console.log("Set record for devopstech.llz");
  txn = await domainContract.setRecord("devopstech", "0x4f9C03863a78D5761bAeF56f330F28DF87aA7BA1");
  await txn.wait();

  const record = await domainContract.getRecord("devopstech");
  console.log("record: ", record);

  const address = await domainContract.getAddress("devopstech");
  console.log("Owner of domain:", address);

  const balance = await hre.ethers.provider.getBalance(domainContract.address);
  console.log("Contract balance:", hre.ethers.utils.formatEther(balance));
};

const runMain = async () => {
  try {
    await main();
    process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

runMain();
