const axios = require("axios");
const fs = require("fs");
const FormData = require("form-data");
const dotenv = require("dotenv");
dotenv.config();

const fileName = "./assets/devops.svg";

const pinFileToIPFS = async () => {
  const url = `https://api.pinata.cloud/pinning/pinFileToIPFS`;
  let data = new FormData();
  data.append("file", fs.createReadStream(fileName));
  const res = await axios.post(url, data, {
    maxContentLength: "Infinity", 
    headers: {
      "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
      pinata_api_key: process.env.PINATA_API_KEY, 
      pinata_secret_api_key: process.env.PINATA_SECRET_API_KEY,
    },
  });
  console.log(res.data);
};

pinFileToIPFS();