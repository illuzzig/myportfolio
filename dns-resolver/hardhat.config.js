require("@nomiclabs/hardhat-waffle");
const dotenv = require("dotenv");
dotenv.config();
//  https://gateway.pinata.cloud/ipfs/QmPZsiJZoWDzj542EDWQu8nJDGZQsZdtviHisiFLWagJEb
task("accounts", "Prints the list of accounts", async () => {
  const accounts = await ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

module.exports = {
  defaultNetwork: "matictest",
  networks: {
    matictest: {
      url: process.env.MATIC_TEST,
      accounts: {mnemonic: process.env.SEED},
    },
  },
  solidity: "0.8.10",
};
