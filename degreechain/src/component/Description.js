import React from 'react';
import Logo from '../images/logo.png';
import Eth from '../images/ethereum.png';
import { Jumbotron } from 'reactstrap';

const Description = props => {
  return (
    <div>
      <Jumbotron>
        <h1 className="display-3">
          <img src={Logo} height="100" alt="icon" /> Degree Chain{' '}
        </h1>
        <p className="lead">
          Proof of Academic Certification based on the
          <a href="https://www.ethereum.org">
            <img src={Eth} height="30" alt="icon" />
          </a>
          Rinkeby Test Network
        </p>
        <hr className="my-2" />
        <p>
          A trusted way to to share an immutable Degree Parchment associated to
          your social security code
        </p>
        <p className="lead" />
      </Jumbotron>
    </div>
  );
};

export default Description;
