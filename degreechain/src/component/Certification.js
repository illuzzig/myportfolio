import React from 'react';

const Certification = ({ info }) => {
  return (
    <div>
      {info.map((item, i) => {
        return (
          <div key={i.toString()}>
            <p>
              <strong>Degree Parchment</strong>
            </p>
            <p>
              <i>Type of Degree: {item[0]}</i>
            </p>
            <p>
              <i>Authority: {item[1]}</i>
            </p>
            <p>
              <i>Degree Class: {item[2]}</i>
            </p>
            <p>
              <i>Study Title: {item[3]}</i>
            </p>
            <p>
              <i>
                Marks: {item[4]}/{item[5]}
              </i>
            </p>
            <p>
              <i>Honors: {item[6].toString()}</i>
            </p>
            <p>
              <i>
                DATE: {item[9]}/{item[8]}/{item[7]}
              </i>
            </p>
          </div>
        );
      })}
    </div>
  );
};

export default Certification;
