import React from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  InputGroup,
  Input,
  Label,
  FormGroup,
  Form
} from 'reactstrap';
import './home.css';
import container from '../ethereum/container';
import web3 from '../ethereum/web3';

class AddCertification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: '',
      messageNum: '',
      modal: false,
      code: '',
      typeof: 'BACHELOR',
      degreeclass: '',
      studytitle: '',
      minmark: '',
      maxmark: '',
      honors: false,
      date: ''
    };

    this.toggle = this.toggle.bind(this);
  }

  fieldControl = () => {
    let date = this.state.date.toString();
    if (
      this.state.code.length === 0 ||
      this.state.degreeclass.length === 0 ||
      this.state.studytitle.length === 0 ||
      this.state.minmark === 0 ||
      this.state.maxmark === 0 ||
      date.length === 0
    ) {
      return true;
    } else {
      return false;
    }
  };
  numControl = () => {
    if (isNaN(this.state.minmark) || isNaN(this.state.maxmark)) {
      return true;
    } else {
      return false;
    }
  };

  commit = async () => {
    if (this.fieldControl()) {
      this.setState({ message: 'fill out the empty fields' });
      setTimeout(() => {
        this.setState({ message: '' });
      }, 3000);
    }
    if (this.numControl()) {
      this.setState({ messageNum: 'incorrect input' });
      setTimeout(() => {
        this.setState({ messageNum: '' });
      }, 3000);
    } else {
      try {
        let accounts = await web3.eth.getAccounts();
        let date = this.state.date.toString();
        let split = date.split('-');
        await container.methods;

        await container.methods
          .addCertification(
            this.state.code.toUpperCase().replace(/ /g, ''),
            this.state.typeof.toUpperCase().replace(/ /g, ''),
            this.state.degreeclass.toUpperCase().replace(/ /g, ''),
            this.state.studytitle.toUpperCase().replace(/ /g, ''),
            Number(this.state.minmark.replace(/ /g, '')),
            Number(this.state.maxmark.replace(/ /g, '')),
            this.state.honors,
            Number(split[0]),
            Number(split[1]),
            Number(split[2])
          )
          .send({ from: accounts[0], gas: '1000000' });
      } catch (e) {
        this.setState({
          message: 'check out input values or install metamask'
        });
        setTimeout(() => {
          this.setState({ message: '' });
        }, 3000);
      }
    }
    console.log(this.state.date);
  };

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
    this.setState({ message: '' });
    this.setState({ messageNum: '' });
    this.setState({ code: '' });
    this.setState({ typeof: 'BACHELOR' });
    this.setState({ degreeclass: '' });
    this.setState({ studytitle: '' });
    this.setState({ minmark: '' });
    this.setState({ maxmark: '' });
    this.setState({ honors: false });
    this.setState({ date: '' });
  }

  render() {
    return (
      <div>
        <Button color="primary" onClick={this.toggle}>
          {this.props.buttonLabel}
        </Button>
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggle}>Add New Certification</ModalHeader>
          <ModalBody>
            <Label for="exampleEmail">Code</Label>
            <InputGroup>
              <Input
                value={this.state.code}
                onChange={event => this.setState({ code: event.target.value })}
                placeholder=""
                className="upper"
              />
            </InputGroup>
            <br />

            <Label for="exampleEmail">Type Of</Label>
            <InputGroup>
              <Input
                value={this.state.typeof}
                onChange={event =>
                  this.setState({ typeof: event.target.value })
                }
                type="select"
                name="select"
                id="exampleSelect"
              >
                <option>BACHELOR</option>
                <option>MASTER</option>
              </Input>
            </InputGroup>
            <br />
            <Label for="exampleEmail">Degree Class</Label>
            <InputGroup>
              <Input
                value={this.state.degreeclass}
                onChange={event =>
                  this.setState({ degreeclass: event.target.value })
                }
                placeholder=""
                className="upper"
              />
            </InputGroup>
            <br />
            <Label for="exampleEmail">Study Title</Label>
            <InputGroup>
              <Input
                value={this.state.studytitle}
                onChange={event =>
                  this.setState({ studytitle: event.target.value })
                }
                placeholder=""
                className="upper"
              />
            </InputGroup>
            <br />
            <Label for="exampleEmail">Marks</Label>
            <Form inline>
              <FormGroup>
                <Input
                  value={this.state.minmark}
                  onChange={event =>
                    this.setState({ minmark: event.target.value })
                  }
                  className="margin"
                />{' '}
                /
              </FormGroup>
              <FormGroup>
                <Input
                  value={this.state.maxmark}
                  onChange={event =>
                    this.setState({ maxmark: event.target.value })
                  }
                  className="marginl"
                  placeholder="110"
                />
              </FormGroup>{' '}
            </Form>
            <h6 className="err">
              <sub>{this.state.messageNum}</sub>
            </h6>
            <FormGroup check inline>
              <Label check>
                <Input
                  value={this.state.honors}
                  onChange={event =>
                    this.setState({ honors: !this.state.honors })
                  }
                  type="checkbox"
                />{' '}
                Honors
              </Label>
            </FormGroup>

            <FormGroup>
              <br />
              <Label for="exampleDate">Date</Label>
              <Input
                value={this.state.date}
                onChange={event => this.setState({ date: event.target.value })}
                type="date"
                name="date"
                id="exampleDate"
                placeholder="date placeholder"
              />
            </FormGroup>
            <h6 className="err">
              <sub>{this.state.message}</sub>
            </h6>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.commit}>
              Submit
            </Button>{' '}
            <Button color="secondary" onClick={this.toggle}>
              Close
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default AddCertification;
