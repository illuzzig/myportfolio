import React from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  InputGroup,
  Input,
  Label
} from 'reactstrap';
import './home.css';

import container from '../ethereum/container';
import web3 from '../ethereum/web3';
class AddAuthority extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      desc: '',
      address: '',
      message: ''
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
      message: '',
      desc: '',
      address: ''
    });
  }

  commit = async () => {
    if (this.state.address.length === 0 || this.state.desc.length === 0) {
      this.setState({ message: 'fill out the empty fields' });
      setTimeout(() => {
        this.setState({ message: '' });
      }, 3000);
    } else {
      try {
        let accounts = await web3.eth.getAccounts();
        await container.methods
          .addAuthority(
            this.state.address.replace(/ /g, ''),
            this.state.desc.toUpperCase().replace(/ /g, '')
          )
          .send({
            from: accounts[0],
            gas: '1000000'
          });

        this.setState({
          modal: !this.state.modals
        });
      } catch (e) {
        this.setState({
          message: 'check out input values or install metamask'
        });
        setTimeout(() => {
          this.setState({ message: '' });
        }, 3000);
      }
    }
  };

  render() {
    return (
      <div>
        <Button color="primary" onClick={this.toggle}>
          {this.props.buttonLabel}
        </Button>
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggle}>Add New Authority</ModalHeader>
          <ModalBody>
            <p>
              The Owner of the master contract is able to add new Authorities
            </p>
            <Label for="examplePassword">Address</Label>
            <InputGroup>
              <Input
                placeholder=""
                value={this.state.address}
                onChange={event =>
                  this.setState({ address: event.target.value })
                }
              />
            </InputGroup>
            <br />
            <Label for="examplePassword">Description</Label>
            <InputGroup>
              <Input
                value={this.state.desc}
                onChange={event => this.setState({ desc: event.target.value })}
                className="upper"
                placeholder=""
              />
            </InputGroup>
            <h6 className="err">
              <sub>{this.state.message}</sub>
            </h6>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.commit}>
              Submit
            </Button>{' '}
            <Button color="secondary" onClick={this.toggle}>
              Close
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default AddAuthority;
