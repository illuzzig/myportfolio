import React from 'react';

import { Container, Row, Col } from 'reactstrap';

import GetInfoModal from './GetInfoModal';
import AddAuthority from './AddAuthority';
import AddCertification from './AddCertification';

import Student from '../images/student.png';
import Authority from '../images/authority.png';
import Certification from '../images/certification.png';
import Footer from '../images/footer.png';

const Description = props => {
  return (
    <div>
      <h1 align="center">QuickStart</h1>
      <br />
      <Container fluid align="center">
        <Row>
          <Col sm="6" md="6" lg="4">
            <img
              className="rounded-circle"
              src={Student}
              width="140"
              alt="icon"
              height="140"
            />
            <h3>Student</h3>
            <p>
              Everyone has the privilege to see Student's certifications thanks
              to an univocal code
            </p>
            <GetInfoModal buttonLabel="Get Information" />
            <br />
            <br />
          </Col>
          <Col sm="6" md="6" lg="4">
            {' '}
            <img
              className="rounded-circle"
              src={Authority}
              width="140"
              alt="icon"
              height="140"
            />
            <h3>Authority</h3>
            <p>
              An Authority is represented by a specific Department or
              University. Only the Owner is able to add a new instance
            </p>
            <AddAuthority buttonLabel="Add Authority" />
            <br />
            <br />
          </Col>
          <Col sm="6" md="6" lg="4">
            {' '}
            <img
              className="rounded-circle"
              src={Certification}
              width="140"
              height="140"
              alt="icon"
            />
            <h3>Certification</h3>
            <p>
              Every certification can only be delivered by an Authority which is
              contained in the approved list
            </p>
            <AddCertification buttonLabel="Add certification" />
            <br />
            <br />
          </Col>
        </Row>
      </Container>
      <br />

      <footer className="footer">
        <h6 align="center">
          Degree Chain | 2018 <br />
          <img src={Footer} height="25" alt="" />
        </h6>
      </footer>
    </div>
  );
};

export default Description;
