import React from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  InputGroup,
  Input
} from 'reactstrap';
import './home.css';
import './home.css';
import container from '../ethereum/container';
import compiledDegree from '../ethereum/universitydegree';

import Certification from './Certification';
class GetInfoModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      cert: false,
      code: '',
      message: '',
      info: '',
      load: ''
    };
    this.toggle = this.toggle.bind(this);
    this.toggleCert = this.toggleCert.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
    this.setState({ code: '' });
  }
  toggleCert() {
    this.setState({
      cert: !this.state.cert
    });
  }

  getInfo = async () => {
    if (this.state.code.length === 0) {
      this.setState({ message: 'empty field' });
      setTimeout(() => {
        this.setState({ message: '' });
      }, 2000);
    } else {
      let studentCertification = [];
      let info = [...this.state.info];
      const list = await container.methods
        .getStudentCertification(
          this.state.code.toUpperCase().replace(/ /g, '')
        )
        .call();

      if (list.length === 0) {
        this.setState({ message: 'code not found' });
        setTimeout(() => {
          this.setState({ message: '' });
        }, 2000);
      } else {
        this.setState({
          load:
            'https://loading.io/spinners/coolors/lg.palette-rotating-ring-loader.gif'
        });
        this.setState({ message: '' });
        for (let i = 0; i < list.length; i++) {
          studentCertification[i] = await compiledDegree(list[i]);

          info[i] = await studentCertification[i].methods.getInfo().call();
        }
        this.setState({ info });

        this.toggleCert();

        this.setState({
          modal: !this.state.modal
        });
        this.setState({
          load: ''
        });
        this.setState({ code: '' });
      }
    }
  };

  render() {
    return (
      <div>
        <Button color="primary" onClick={this.toggle}>
          {this.props.buttonLabel}
        </Button>
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggle}>
            Get Student Information
          </ModalHeader>
          <ModalBody>
            <p>Enter his/her social security code</p>
            <InputGroup>
              <Input
                placeholder=""
                className="upper"
                value={this.state.code}
                onChange={event => this.setState({ code: event.target.value })}
              />
            </InputGroup>
            <h6 className="err">
              <sub>{this.state.message}</sub>
            </h6>
          </ModalBody>

          <ModalFooter>
            <img height="50" alt="" align="center" src={this.state.load} />
            <Button color="primary" onClick={this.getInfo}>
              Submit
            </Button>{' '}
            <Button color="secondary" onClick={this.toggle}>
              Close
            </Button>
          </ModalFooter>
        </Modal>

        <Modal
          isOpen={this.state.cert}
          toggle={this.toggleCert}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggleCert}>
            List of Certifications
          </ModalHeader>
          <ModalBody>
            <Certification info={this.state.info} />
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleCert}>
              Close
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default GetInfoModal;
