import React from 'react';
import NavBar from './component/NavBar';
import Description from './component/Description';
import Quickstart from './component/Quickstart';

const App = () => {
  return (
    <div>
      <NavBar />
      <Description />
      <Quickstart />
    </div>
  );
};

export default App;
