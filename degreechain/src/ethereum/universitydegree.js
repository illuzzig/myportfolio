import web3 from './web3';
import UniversityDegree from './build/UniversityDegree.json';

export default address => {
  return new web3.eth.Contract(JSON.parse(UniversityDegree.interface), address);
};
