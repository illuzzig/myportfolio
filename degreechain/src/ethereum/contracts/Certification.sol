pragma solidity ^0.4.17;
 //"LLZGPP95T28L109Z", "BACHELOR", "L31 - CLASSE DELLE LAUREE IN SCIENZE E TECNOLOGIE INFORMATICHE", "INFORMATICA E TECNOLOGIE PER LA PRODUZIONE DEL SOFTWARE", 104, 110, false, 2017, 12, 12
contract Container{

    address public owner;

    mapping(address => string) private authority;
    mapping(bytes32 => Certification []) private list;

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }
    function Container() public {
        owner = msg.sender;
    }

    function changeOwner(address newOwner) public onlyOwner{
        owner = newOwner;
    }

    function addAuthority(address _universityAddress, string info) public
    onlyOwner{
        require((bytes(authority[_universityAddress]).length ) == 0);
        authority[_universityAddress] = info;
    }

    function getAuthorityDescription(address _universityAddress) public view
    returns(string) {
        require((bytes(authority[_universityAddress]).length ) != 0);
        return authority[_universityAddress];
    }

    function addCertification(string _id, string _typeOf, string
    _degreeClass, string _studyTitle, uint _marks, uint _maxValueMarks,
    bool _honors, uint16 _year, uint8 _month, uint8 _day) public {

        require((bytes(authority[msg.sender]).length ) != 0);

        UniversityDegree degree = new UniversityDegree(_typeOf, _degreeClass,
            _studyTitle, authority[msg.sender], _marks, _maxValueMarks,
            _honors, _year, _month, _day);

        list[keccak256(bytes(_id))].push(degree);
    }

    function getStudentCertification(string _id) public view returns (Certification []){
        require(list[keccak256(bytes(_id))].length != 0);
        return list[keccak256(bytes(_id))];
    }

}

contract Certification {

    struct Date {
        uint16 year;
        uint8 month;
        uint8 day;
    }

    Date internal date;
    string internal typeOf;
    string internal authority;
    string internal studyTitle;

    function Certification(string _typeOf, string _auth, string _studyTitle, uint16 _year,
    uint8 _month, uint8 _day) public {
       typeOf= _typeOf;
       authority= _auth;
       studyTitle = _studyTitle;
       date = Date({
           year: _year,
           month: _month,
           day : _day
       });
    }
}


contract UniversityDegree is Certification {

    string private degreeClass;
    uint private marks;
    uint private maxValueMarks;
    bool private honors;
    function UniversityDegree(string _typeOf, string _degreeClass, string _studyTitle, string
    _auth, uint _marks, uint _maxValueMarks,
    bool _honors, uint16 _year, uint8 _month, uint8 _day)
    Certification(_typeOf, _auth,_studyTitle, _year,_month, _day) public {
       degreeClass = _degreeClass;

       marks= _marks;
       maxValueMarks= _maxValueMarks;
       honors= _honors;
    }

    function getInfo() public view returns(string, string, string, string, uint,
    uint, bool, uint16, uint8, uint8){
        return (typeOf, authority, degreeClass, studyTitle, marks, maxValueMarks,
        honors, date.year, date.month, date.day);
    }
}
