import web3 from './web3';
import Container from './build/Container.json';

const instance = new web3.eth.Contract(
  JSON.parse(Container.interface),
  '0x9ff06945c0c28306cbe22653cf4149af048c17c4'
);

export default instance;
