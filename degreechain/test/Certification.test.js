const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());
const compiledContainer = require('../src/ethereum/build/Container.json');
const compiledDegree = require('../src/ethereum/build/UniversityDegree.json');

let accounts;
let container;
let degreeAddress;
let degree;
let authorityDescription;

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  container = await new web3.eth.Contract(
    JSON.parse(compiledContainer.interface)
  )
    .deploy({ data: compiledContainer.bytecode })
    .send({ from: accounts[0], gas: '2000000' });

  await container.methods.addAuthority(accounts[1], 'uniba').send({
    from: accounts[0],
    gas: '1000000'
  });
  authorityDescription = await container.methods
    .getAuthorityDescription(accounts[1])
    .call();

  await container.methods
    .addCertification(
      'LLZGPP95T28L109Z',
      'BACHELOR',
      'SW DEV',
      'ITPS',
      104,
      110,
      false,
      2017,
      12,
      28
    )
    .send({ from: accounts[1], gas: '1000000' });

  await container.methods
    .addCertification(
      'LLZGPP95T28L109Z',
      'MASTER',
      'SW ENGINEER',
      'IT EXPERT',
      104,
      110,
      false,
      2018,
      12,
      12
    )
    .send({ from: accounts[1], gas: '1000000' });
});

describe('Degree Test', () => {
  it('check container', async () => {
    assert.ok(container.options.address);
  });

  it('Authority description', async () => {
    assert.equal(authorityDescription, 'uniba');
  });

  it('Not an Authority', async () => {
    try {
      fakeAuthority = await container.methods
        .getAuthorityDescription(accounts[4])
        .call();
      assert(false);
    } catch (error) {
      assert(error);
    }
  });

  it('Owner check', async () => {
    const owner = await container.methods.owner().call();
    assert.equal(accounts[0], owner);
  });

  it('Change of ownership', async () => {
    await container.methods
      .changeOwner(accounts[2])
      .send({ from: accounts[0], gas: '1000000' });
    const newOwner = await container.methods.owner().call();
    assert.equal(accounts[2], newOwner);
  });

  it('Student info', async () => {
    const list = await container.methods
      .getStudentCertification('LLZGPP95T28L109Z')
      .call();

    studentCertification = await new web3.eth.Contract(
      JSON.parse(compiledDegree.interface),
      list[0]
    );

    const info = await studentCertification.methods.getInfo().call();
    console.log(info);
    assert.equal(list.length, 2);
    assert.equal(info[0], 'BACHELOR');
    console.log('boolean');
    console.log(info[6]);
  });

  it('Not contains ID', async () => {
    try {
      await container.methods.getStudentCertification('FAKE_ID').call();
      assert(false);
    } catch (e) {
      assert(e);
    }
  });
});
